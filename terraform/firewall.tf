resource "hcloud_firewall" "vitess_firewall" {
  name = "vitess_firewall"

  rule {
    direction = "in"
    protocol  = "icmp"
    source_ips = [
      "10.0.0.0/8",
      "192.168.0.0/16",
      "172.16.0.0/12",
      "94.45.133.234/32",
      "194.183.171.25/32",
      "213.174.150.0/24"
    ]
  }

  rule {
    direction = "in"
    protocol  = "tcp"
    port      = "any"
    source_ips = [
      "10.0.0.0/8",
      "192.168.0.0/16",
      "172.16.0.0/12",
      "94.45.133.234/32",
      "194.183.171.25/32",
      "213.174.150.0/24"
    ]
  }

  rule {
    direction = "in"
    protocol  = "udp"
    port      = "any"
    source_ips = [
      "10.0.0.0/8",
      "192.168.0.0/16",
      "172.16.0.0/12",
      "94.45.133.234/32",
      "194.183.171.25/32",
      "213.174.150.0/24"
    ]
  }

  rule {
    direction = "out"
    protocol  = "tcp"
    port      = "any"
    destination_ips = [
      "0.0.0.0/0"
    ]
  }

  rule {
    direction = "out"
    protocol  = "udp"
    port      = "any"
    destination_ips = [
      "0.0.0.0/0"
    ]
  }
}
