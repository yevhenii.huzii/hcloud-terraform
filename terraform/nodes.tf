resource "hcloud_server" "vitess" {
  count       = var.nodes_count
  name        = "vitess-${count.index}"
  image       = var.hcloud_image
  server_type = var.vitess_server_type
  location    = var.vitess_location
  ssh_keys    = [var.hcloud_ssh_key]
  user_data   = file("user_data.yml")

  public_net {
    ipv4_enabled = true
    ipv6_enabled = false
  }

  network {
    network_id = var.network_id
  }

  labels = {
      firewall-attachment = "vitess_servers"
  }

}

resource "hcloud_firewall_attachment" "fw_ref" {
    firewall_id = hcloud_firewall.vitess_firewall.id
    label_selectors = ["firewall-attachment=vitess_servers"]
}
