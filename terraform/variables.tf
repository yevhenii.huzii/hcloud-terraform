variable "hcloud_token" {
  default     = ""
  description = "Hetzner Cloud API token"
}  

# Vitess node vars
variable "vitess_name" {
  default     = "vitess-01"
  description = "Name of the vitess node"
}

variable "vitess_server_type" {
  default     = "cpx51"
  description = "Server type of the vitess node"
}

variable "vitess_location" {
  default     = "ash"
  description = "Location of the vitess node"
}

variable "nodes_count" {
  default     = 3
  description = "Number of vitess nodes"
}

variable "hcloud_image" {
  default     = "ubuntu-20.04"
}

variable "hcloud_ssh_key" {
  default     = "yevhenii.huzii@maklai.com.ua"
}

variable "network_id" {
  default     = 2213003
}
